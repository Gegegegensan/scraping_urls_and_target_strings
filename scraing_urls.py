# coding: utf-8
import requests
from bs4 import BeautifulSoup
import csv

# x = 1
# url_output = []
# info_output = []

def list_urls():
    x = 1
    url_output = []
    while x < 35: # Change this number accordingly depending on how many pages you want to scrape
        page = "<uri>" + str(x) # Add a directory name that represents next pages
        url = "<url>" + page # Paste the URL you want to scrape
        headers = {'User-Agent':'Mozilla/5.0'}
        r = requests.get(url, headers=headers)
        soup = BeautifulSoup(r.text, "lxml")
        for atag in soup.find_all("a"): # Paste the class names here
            link = atag.get("href")
            #if "<SPECIFIC STRING>" in link:
            #    link.replace("<SPECIFIC STRING>", "")
            name = atag.get_text()
            school = soup.title.decode()[14:16]
            if school == name[-2:] or name[-2:] == "":
                if str(link) != "":
                    if name != "":
                        output = "<url>" + str(link), name # Paste the URL you want to scrape since link only shows directory names
                        url_output.append(output)
        x = x + 1
    with open("urls.csv", 'w', encoding='utf-8') as f:
        writer = csv.writer(f, lineterminator="\n")
        writer.writerows(url_output)



def list_info():
    info_output = []
    with open('urls.csv', encoding='utf-8') as f:
        lines = f.readlines()
    for line in lines:
        headers = {'User-Agent': 'Mozilla/5.0'}
        r = requests.get(line[:-2], headers=headers)
        soup = BeautifulSoup(r.text, 'lxml')
        for info in soup.find_all('td', class_=''): # Replace <SPECIFY> with necessary HTML tag and class name
            address_info = info.get_text()
            school = soup.title.decode()[7:17]
            output = school, address_info, line
            info_output.append(output)
    with open("urls-detailed-info.csv", 'w', encoding='utf-8') as f:
        writer = csv.writer(f, lineterminator="\n")
        writer.writerows(info_output)



if __name__ == '__main__':
    list_urls()
    list_info()
